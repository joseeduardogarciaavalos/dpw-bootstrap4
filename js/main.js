$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2500
    });
    $('#contacto').on("show.bs.modal", function(e){
        console.log("El modal comenzó aparecer")
        $('#contactobtn').prop("disabled",true).prop("style","background-color:red");
    });
    $('#contacto').on("shown.bs.modal", function(e){
        console.log("El modal ha aparecido")
    });
    $('#contacto').on("hide.bs.modal",function(e){
        console.log("El modal comenzó ocultarse")
    });
    $('#contacto').on("hidden.bs.modal", function(e){
        console.log("El modal se ha ocultado")
        $('#contactobtn').removeAttr("disabled").removeAttr("style");
    });
})